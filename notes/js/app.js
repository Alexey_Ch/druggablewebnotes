angular.module('myApp', []).
    service('profileService', function() {
        var profile = {name:'', email:''};
        this.getProfile = function() {
            return this.profile;
        }
        this.setProfile = function(profile) {
            this.profile = profile;
        }
    }).
    service('notesService', function () {
        var data = [
            {id:1, title:'Note 1', text:'Note text'},
            {id:2, title:'Note 2', text:'Note text'},
            {id:3, title:'Note 3', text:'Note text'},
            {id:4, title:'Note 4', text:'Note text'},
            {id:5, title:'Note 5', text:'Note text'},
            {id:6, title:'Note 6', text:'Note text'},
            {id:7, title:'Note 7', text:'Note text'},
            {id:8, title:'Note 8', text:'Note text'}
        ];

        this.notes = function () {
            return data;
        }
        this.getEmptyIndex = function() {
            var ids = [];
            angular.forEach(data, function (note) {
                ids.push(note.id);
            });
            ids.sort();
            return ids[ids.length - 1] + 1;
        }
        this.addNote = function (note) {
            if(note.id == -1) {
                var currentIndex = this.getEmptyIndex();
                note.id = currentIndex;
            }
            data.push(note);
        }
        this.getNote = function (id) {
            var neededNote = {};
            angular.forEach(data, function (note) {
                if (note.id == id){
                    neededNote = note;
                    return;
                }
            });
            return neededNote;
        }
        this.deleteNote = function (id) {
            var oldNotes = data;
            data = [];
            angular.forEach(oldNotes, function (note) {
                if (note.id !== id) data.push(note);
            });
        }
    })
    .directive('myNotebook', function ($log) {
        return {
            restrict:"E",
            scope:{
                notes:'=',
                ondelete:'&'
            },
            templateUrl:"partials/notebook-directive.html",
            controller:function ($scope, $attrs) {
                $scope.deleteNote = function (id) {
                    $scope.ondelete({id:id});
                }
            }
        };
    })
    .directive('myNote', function () {
        return {
            restrict:'E',
            scope:{
                delete:'&',
                note:'='
            },
            link:function (scope, element, attrs) {
                // Since this project pulls in jQuery, element is already
                // a wrapped jQuery object, so there is no need to do
                // something like:  var $el = $(element);
                // We can use element directly with jQuery methods.

                element.hide().fadeIn('slow');

                $('.thumbnails').sortable({
                    placeholder:"ui-state-highlight", forcePlaceholderSize:true
                });
            }
        };
    }).
    directive('draggable', function($document) {
    return function(scope, element, attr) {
        var startX = 0, startY = 0, x = 0, y = 0;
        element.css({
            position: 'relative',
            /*border: '1px solid red',
            backgroundColor: 'lightgrey',*/
            cursor: 'pointer'
        });
        element.bind('mousedown', function(event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            startX = event.screenX - x;
            startY = event.screenY - y;
            $document.bind('mousemove', mousemove);
            $document.bind('mouseup', mouseup);
        });

        function mousemove(event) {
            y = event.screenY - startY;
            x = event.screenX - startX;
            element.css({
                top: y + 'px',
                left:  x + 'px'
            });
        }

        function mouseup() {
            $document.unbind('mousemove', mousemove);
            $document.unbind('mouseup', mouseup);
        }
    }
}).
  /*controller('NotebookCtrl', ['$scope', 'notesService', function ($scope, notesService) {
    $scope.getNotes = function () {
        return notesService.notes();
    };
    $scope.addNote = function (note) {
        note.id = -1;
        if(note.title != '') {
            notesService.addNote(note);
        }
    };

    $scope.deleteNote = function (id) {
        notesService.deleteNote(id);
    };

    $scope.resetForm = function() {
        $scope.noteTitle = '';
    };

    $scope.notes = notesService.notes();
    $scope.note = {};
}])*/
    controller('NotebookCtrl', ['$scope', 'notesService', function ($scope, notesService) {
        $scope.getNotes = function () {
            return notesService.notes();
        };

        /*$scope.addNote = function (noteTitle) {
            if(noteTitle != '') {
                notesService.addNote(noteTitle);
            }
        };*/
        $scope.addNote = function (note) {
            note.id = -1;
            if(note.title != '') {
                notesService.addNote(note);
            }
        };

        $scope.deleteNote = function (id) {
            notesService.deleteNote(id);
        };

        $scope.resetForm = function() {
            $scope.noteTitle = '';
        };
    }])
    .
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/new', {templateUrl: 'partials/edit.html', controller: 'NotebookCtrl'}).
            when('/edit/:id', {templateUrl: 'partials/edit.html', controller: 'EditController'}).
            when('/profile', {templateUrl: 'partials/profile.html', controller: 'ProfileController'}).
            when('/', {templateUrl: 'partials/list.html'}).
            otherwise('/')
    }]);